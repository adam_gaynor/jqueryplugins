(function() {

  $.Tabs = function (el) {
    this.$el = $(el);
    this.$activeTab = this.$el.find(".active");

    var contentLocation = this.$el.data("contentTabs");

    this.$contentPanes = $(contentLocation);
    this.$activePane = this.$contentPanes.find(".active");
    this.bindListeners();
    //this.$el.on('click', 'a', this.clickTab.bind(this));
  };

  $.Tabs.prototype.clickTab = function(event){
    event.preventDefault();
    var $targetTab = $(event.currentTarget);
    if (!$targetTab.hasClass("active")){
      this.$activeTab.removeClass("active");
      this.$activeTab = $targetTab;
      this.$activeTab.addClass("active");
      this.$activePane.addClass("transitioning");
      this.$contentPanes.one('transitionend', ".tab-pane", this.transitionPane.bind(this));
    }
  }

  $.Tabs.prototype.transitionPane = function(event){
    this.$activePane.toggleClass("active transitioning");
    var reference = this.$activeTab.attr("href");
    this.$activePane = this.$contentPanes.find(reference);
    this.$activePane.addClass("active transitioning");
    setTimeout(function(){
      this.$activePane.removeClass("transitioning");
    }.bind(this) ,0);
  }

  $.Tabs.prototype.bindListeners = function() {
    this.$el.on('click', "a", this.clickTab.bind(this));

  }

  $.fn.tabs = function () {
    return this.each(function () {
      new $.Tabs(this);

    });
  };


})();
