(function(){
  $.Thumbnails = function(el){
    this.$el = $(el);
    this.$images = this.$el.find(".gutter-images").children();
    this.$activeImage = this.$images.first();
    this.activate(this.$activeImage);
    this.gutterIdx = 0;
    this.$gutterImages;
    this.fillGutterImages();
    this.bindEvents();
  }

  $.Thumbnails.prototype.fillGutterImages = function(){
    this.$gutterImages = $();
    this.$el.find(".gutter-images").empty();
    this.$images.each(function(idx, img){
      if((idx >= this.gutterIdx) && (idx < this.gutterIdx + 5)){
        this.$gutterImages.push($(img));
        this.$el.find(".gutter-images").append($(img));
      }
    }.bind(this));

    // this.$el.find(".gutter-images").append(this.$gutterImages);
  }

  $.Thumbnails.prototype.bindEvents = function(){
    this.$images.on("mouseenter", this.setBigImage.bind(this));
    this.$images.on("mouseleave", function() {
      this.activate(this.$activeImage);
    }.bind(this));
  }

  $.Thumbnails.prototype.activate = function($img){
    this.$el.find(".active").html($img.clone());
  }

  $.Thumbnails.prototype.setBigImage = function (event) {
    this.activate($(event.currentTarget));
  }

  $.fn.thumbnail = function () {
    return this.each(function () {
      new $.Thumbnails(this);
    });
  };

})();
