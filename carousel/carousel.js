(function () {


  $.Carousel = function(el) {
    this.$el = $(el);
    this.activeIdx = 0;
    this.$pictures = this.$el.find("img")
    this.$pictures.eq(0).addClass("active");
    this.bindListeners();
  }

  $.Carousel.prototype.bindListeners = function(){
    this.$el.find(".slide-left").on('click', this.slide.bind(this, "left"));
    this.$el.find(".slide-right").on('click', this.slide.bind(this, "right"));
  }


  $.Carousel.prototype.slide = function(direction){
    // this.$pictures.eq(this.activeIdx).removeClass("active");
    this.$pictures.eq(this.activeIdx).one("transitionend", this.inactivate.bind(this, this.activeIdx, direction));
    switch (direction) {
      case "left":
        this.$pictures.eq(this.activeIdx).addClass("right");
        this.activeIdx--;
        if (this.activeIdx < 0){
          this.activeIdx += this.$pictures.length;
        };
        this.$pictures.eq(this.activeIdx).addClass("left");
        break;
      case "right":
        this.$pictures.eq(this.activeIdx).addClass("left");
        this.activeIdx++;
        if (this.activeIdx > this.$pictures.length - 1){
          this.activeIdx = 0;
        };
        this.$pictures.eq(this.activeIdx).addClass("right");
        break;
      }

    this.$pictures.eq(this.activeIdx).addClass("active");
    setTimeout(function() {
      this.$pictures.eq(this.activeIdx).removeClass("left right");
    }.bind(this), 0)
  }

  $.Carousel.prototype.inactivate = function(idx, direction,event){

    switch(direction){
      case "left":
        this.$pictures.eq(idx).addClass("right");
        break;
      case "right":
        this.$pictures.eq(idx).addClass("left");
        break;
    }

    setTimeout(function() {
      this.$pictures.eq(idx).removeClass("left right active");
    }.bind(this), 0)

  }

  $.fn.carousel = function () {
    return this.each(function () {
      new $.Carousel(this);
    });
  };

})();
